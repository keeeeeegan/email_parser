#!/usr/bin/env ruby

# Author: Keegan Berry
# Since: 03/4/12

require './classes/EmailParser.rb'

# get filenames from input argument
input_filename, output_filename = ARGV

# check to make sure the files are specified
if (input_filename == nil)
	puts "Error, input file not specified."
	Process.exit(1)
end

if (output_filename == nil)
	puts "Error, output file not specified."
	Process.exit(1)
end

#open the output file
output = File.open(output_filename, 'w')

# initialize the parser
parse = EmailParser::Parser.new()

# send the parser the filename
parse.read_file(input_filename)

# tell the parser to parse the text
parse.parse_text()

# write parsed data to the output file (as long as 
#	values are not null)

output.write("HEADER\n\n")

unless parse.email.mime.nil?
	output.write("MIME Version:#{parse.email.mime}\n")
end

unless parse.email.to.nil?
	output.write("To:#{parse.email.to}\n")
end

unless parse.email.cc.nil?
	output.write("CC:#{parse.email.cc}\n")
end

unless parse.email.bcc.nil?
	output.write("BCC:#{parse.email.bcc}\n")
end

unless parse.email.delivered_to.nil?
	output.write("Delivered To:#{parse.email.delivered_to}\n")
end

unless parse.email.from.nil?
	output.write("From:#{parse.email.from}\n")
end

unless parse.email.date.nil?
	output.write("Date:#{parse.email.date}\n")
end

unless parse.email.subject.nil?
	output.write("Subject:#{parse.email.subject}\n")
end

unless parse.email.return_path.nil?
	output.write("Return Path:#{parse.email.return_path}\n")
end

unless parse.email.message_id.nil?
	output.write("Message ID:#{parse.email.message_id}\n")
end

unless parse.email.in_reply_to.nil?
	output.write("In Reply To ID:#{parse.email.in_reply_to}\n")	
end

unless parse.email.archived_at.nil?
	output.write("Archived At:#{parse.email.archived_at}\n")
end

unless parse.email.auto_submitted.nil?
	output.write("Auto Submitted:#{parse.email.auto_submitted}\n")
end

output.write("\n\nBODY\n\n")

# send to the output each of the bodies, specifying the type
if parse.email.bodies != nil and parse.email.bodies.count > 0
	parse.email.bodies.each do |type, body|	
		output.write("------------------------------\nType #{type}\n------------------------------\n#{body}\n\n")
	end	
else
	output.write("No body text found.")
end

puts "Output saved to #{output_filename}."

output.close()
