<?php
/**
 * Email
 * Contains related fields and body text(s).
 *
 * @version 	1.0
 * @author 		Keegan Berry
 * @since		03/03/12
 */

class Email
{

	/**
	 * public email fields
	 */
	public $mime;
	public $to;
	public $cc;
	public $bcc;
	public $delivered_to;	
	public $from;
	public $date;
	public $subject;
	public $return_path;
	public $message_id;
	public $in_reply_to;
	public $archived_at;
	public $auto_submitted;

	/**
	 * private array Email's array of body text(s)
	 */
	private $bodies;

	/**
	 * Creates the instance of the email, you can optionally pass in the field values
	 *
	 * @param string mime
	 * @param string to	 
	 * @param string delivered_to 
	 * @param string from 
	 * @param string cc 
	 * @param string bcc 
	 * @param string return_path 
	 * @param string message_id 
	 * @param string in_reply_to 
	 * @param string date 
	 * @param string subject 
	 * @param string archived_at 
	 * @param string auto_submitted 
	 * @param array bodies
	 *
	 */
	function __construct($mime = null, $to = null, $delivered_to = null, $from = null, 
						$cc = null, $bcc = null, $return_path = null, $message_id = null,
						$in_reply_to = null, $date = null, $subject = null, $archived_at = null,
						$auto_submitted = null, $bodies = null)
	{	
		$this->mime = $mime;
		$this->to = $to;		
		$this->delivered_to = $delivered_to;
		$this->from = $from;
		$this->cc = $cc;
		$this->bcc = $bcc;
		$this->return_path = $return_path;
		$this->message_id = $message_id;
		$this->in_reply_to = $in_reply_to;
		$this->date = $date;
		$this->subject = $subject;
		$this->auto_submitted = $date;
		$this->archived_at = $subject;
		$this->bodies = $bodies;
	}

	/**
	 * Getter function to get the array of body text(s)
	 *
 	 * @param string body text as string
 	 *
	 */
	public function addBody($body)
	{
		$this->bodies[] = $body;	
	}

	/**
	 * Getter function to get the array of body text(s)
	 *
 	 * @return array array of body text(s)
	 */
	public function getBodies() {
		return $this->bodies;
	}
}

?>