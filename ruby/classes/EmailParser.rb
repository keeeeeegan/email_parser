#!/usr/bin/env ruby

# Email Parser
# Takes in an email as a string, parses it, and creates an Email instance
#
# Author: Keegan Berry
# Since: 03/4/12

module EmailParser

	# Contains related fields and body text(s).
	class Email

		# declare all stored fields as get-able and set-able
		attr :mime, true
		attr :delivered_to, true		
		attr :to, true
		attr :cc, true
		attr :bcc, true	
		attr :from, true
		attr :date, true
		attr :subject, true
		attr :return_path, true
		attr :message_id, true
		attr :in_reply_to, true	
		attr :bodies, true	
		attr :archived_at, true	
		attr :auto_submitted, true	

		# initilize the bodies hash
		def initialize
			@bodies = Hash.new
		end
	end	

	# Takes a filename (an email), opens the file, parses it, and creates an Email instance
	class Parser

		# class constants fields to search for
		@@MIME = "MIME-Version"
		@@DELIVERED_TO = "Delivered-To"
		@@TO = "To"
		@@CC = "Cc"
		@@BCC = "Bcc"	
		@@FROM = "From"
		@@DATE = "Date"
		@@SUBJECT = "Subject"
		@@RETURN_PATH = "Return-Path"
		@@MESSAGE_ID = "Message-ID"
		@@IN_REPLY_TO = "In-Reply-To"
		@@ARCHIVED_AT = "Archived-At"
		@@AUTO_SUBMITTED = "Auto-Submitted"

		# stored email data, including the Email instance
		# don't want the content or fields to be accessed outside the class
		attr_reader :content
		attr_reader :fields
		attr :email, true

		# opens the file and initializes some variables
		def read_file(filename)
			# open files
			file = File.open(filename, 'r')

			# read in content of file
			@content = file.read()
			file.close()

			@fields = Hash.new
			@email = Email.new
		end

		# calls relevant parsing functions
		def parse_text()
			find_fields()
			find_values()
			find_bodies()
		end	

		# search for all fields that match the pattern (not all will be useful)
		private
		def find_fields()
			@content.scan(/^([A-Z].*?):(.*)/).each do |match|
				@fields[match[0]] = match[1]
			end
		end

		# look through each match and compare it to email constants
		# 	if one of the fields matches, add the value to the Email
		private
		def find_values()
			@fields.each do |field, value|
				if field == @@MIME
					@email.mime = value		
				elsif field == @@TO
					@email.to = value			
				elsif field == @@DELIVERED_TO
					@email.delivered_to = value	
				elsif field == @@CC
					@email.cc = value
				elsif field == @@BCC
					@email.bcc = value	
				elsif field == @@FROM
					@email.from = value	
				elsif field == @@DATE
					@email.date = value			
				elsif field == @@SUBJECT
					@email.subject = value	
				elsif field == @@RETURN_PATH
					@email.return_path = value
				elsif field == @@MESSAGE_ID
					@email.message_id = value
				elsif field == @@IN_REPLY_TO
					@email.in_reply_to = value		
				elsif field == @@ARCHIVED_AT
					@email.archived_at = value	
				elsif field == @@AUTO_SUBMITTED
					@email.auto_submitted = value
				end																																			
			end
		end

		# search through the content of the email and look for a boundary we can use to
		# 	extract any body text
		private
		def find_bodies()

			boundary = @content.match(/boundary="?(.*?)["?|\n]/)
			
			if (boundary != nil)
				boundary = boundary[1].chomp()
				#puts "\n\nOur boundary is #{boundary}\n\n"

				# search for content contained within a set of boundaries
				@content.scan(/(?<=#{boundary})(.*?)(?=#{boundary})/m).each do |body|
					
					# see if the content contains a content type
					content_type = body.to_s.scan(/Content-Type:.*?([a-z].*?\/[a-z].*?);/m)

					# add the content to the Email
					if (content_type[0] != nil and content_type[0] != "")

						@email.bodies[content_type[0][0]] = strip_junk(body[0])
					end
				end
			else
				# some emails do not contain a boundary... the following at least gets the content (relying on the \r\n\r\n)
				matches = @content.split(/\r\n\r\n/m)
				
				unless matches.nil?
					@email.bodies["assumed text/plain"] = strip_junk(matches[1])
				end
			end
		end

		# attempts to strip out extra junk found in body text(s)
		def strip_junk(body)
			return body.gsub(/(=\r)|(--$)/m, "");
		end

	end
end
