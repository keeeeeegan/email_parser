<?php
/**
 * EmailParser
 * Takes in an email as a string, parses it, and creates an Email instance
 *
 * @version 	1.0
 * @author 		Keegan Berry
 * @since		03/03/12
 */

require_once('Email.php');

class EmailParser
{

	/**
	 * const constants used to specify which fields to look for when parsing the email
	 */	
	const MIME = "MIME-Version";
	const DELIVERED_TO = "Delivered-To";
	const TO = "To";
	const CC = "Cc";
	const BCC = "Bcc";		
	const FROM = "From";
	const DATE = "Date";
	const SUBJECT = "Subject";
	const RETURN_PATH = "Return-Path";
	const MESSAGE_ID = "Message-ID";
	const IN_REPLY_TO = "In-Reply-To";	
	const ARCHIVED_AT = "Archived-At";	
	const AUTO_SUBMITTED = "Auto-Submitted";

	/**
	 * Email file's content stored as text
	 */
	private $email_content;
	
	/**
	 * Email instance
	 */	
	public $email;

	function __construct($filename = null)
	{	
		$this->email = new Email();	

		if ($filename != null)
		{
			$this->parseEmailFromFile($filename);
		}			
	}

	/**
	 * Parses the email based on a text file specified
	 *
	 * @param string filename of file to be parsed	 
	 */
	public function parseEmailFromFile($filename)
	{
		// open the file, return an error if it's not found
		$email_handle = @fopen($filename, "r") or die("Error, file not found.\n");
		
		// continue only if the file is found
		if ($email_handle) {
			$email_filesize = filesize($filename);
			$this->email_content = fread($email_handle, $email_filesize);
			fclose($email_handle);
		}

		$this->parseEmail();
	}

	/**
	 * Parses the email based on a string of email text
	 *
	 * @param string string to parse 
	 */
	public function parseEmailFromText($text)
	{
		$this->email_content = $text;
		$this->parseEmail();
	}	

	/**
	 * Parses the email text and creates an Email instance
	 *
	 */
	private function parseEmail()
	{
		// first, search for field types
		preg_match_all("/([A-Z].*?):(.*)/", $this->email_content, $email_matches);

		// go through the field types that are found
		for ($count = count($email_matches[0]), $i = 0; $i < $count; $i++) {
		
			$field = $email_matches[1][$i];
			$value = $this->sanitizeString($email_matches[2][$i]);			

			// if the field type matches one of the const fields, add it to the Email instance
			switch($field)
			{
				case $this::MIME:
					$this->email->mime = $value;
					break;	
				case $this::DELIVERED_TO:
					$this->email->delivered_to = $value;
					break;						
				case $this::TO:
					$this->email->to = $value;
					break;		
				case $this::FROM:
					$this->email->from = $value;	
					break;
				case $this::CC:
					$this->email->cc = $value;
					break;
				case $this::BCC:
					$this->email->bcc = $value;
					break;				
				case $this::RETURN_PATH:
					$this->email->return_path = $value;
					break;
				case $this::DATE:
					$this->email->date = $value;
					break;
				case $this::SUBJECT:
					$this->email->subject = $value;
					break;
				case $this::MESSAGE_ID:
					$this->email->message_id = $value;
					break;
				case $this::IN_REPLY_TO:
					$this->email->in_reply_to = $value;
					break;	
				case $this::ARCHIVED_AT:
					$this->email->archived_at = $value;
					break;	
				case $this::AUTO_SUBMITTED:
					$this->email->auto_submitted = $value;
					break;	
			}
		}

		// second, try to separate the content from the header
		$email_halves = preg_split("/\r\n\r\n/", $this->email_content, 2);
		
		if (count($email_halves) > 0)
		{
			$email_body = $email_halves[1];
		}
		else
		{
			// if the separation was unsuccessful, just use the whole email when comparing
			$email_body = $this->email_content;
		}

		// try to look through the email for the boundry (we need this because it encapsulates body text) 
		preg_match('/boundary="?(.*?)["?|\n]/', $this->email_content, $boundary);

		// only continue if a boundry was successfully found
		if (count($boundary) > 0)
		{
			// once we have the boundary, grab each body available in the email (need to for text within boundries)
			preg_match_all("/(?<=" . trim($boundary[1]) . ")(.*?)(?=" . trim($boundary[1]) . ")/s", $email_body, $body_matches);		

			// go through each found body text, and see if it contains a Content-Type
			for ($body_count = count($body_matches[0]), $i = 0; $i < $body_count; $i++) {
				$match = $body_matches[1][$i];

				// if a Content-Type is found, add the body to the body array
				if (preg_match("/Content-Type:.*?([a-z].*?\/[a-z].*?);/", $match, $type))
				{
					$match = $this->sanitizeString($match);
					$match = $this->stripBodyJunk($match);
					$this->email->addBody(array("type" => $type[1], "content" => $match));
				}
			}
		}
		// some emails do not contain a boundary... the following at least gets the content (relying on the \r\n\r\n)
		else {
			$this->email->addBody(array("type" => "assumed text/plain", "content" => $email_body));
		}
				
	}

	/**
	 * Formats special characters and sanitizes the string by calling php sanitize functions
	 *
	 * @param string string to sanitize
	 *
 	 * @return string sanitized string
	 */
	private function sanitizeString($string_to_sanitize)
	{
		$string_to_sanitize = htmlspecialchars($string_to_sanitize);
		return $string_to_sanitize;
	}

	/**
	 * Attempts to find junk in the body and remove it.
	 *
	 * @param string body string to clean
	 *
 	 * @return string cleaned string, or the original string
	 */
	private function stripBodyJunk($body)
	{
		// search the body text for content junk that should be removed
		$new_body = preg_replace("/(=\r)|(--$)/s", "", $body);

		return trim($new_body);
	}

	/**
	 * Getter function to get the contents of the email
	 *
 	 * @return string email content as string
	 */
	public function getEmailAsText()
	{
		return $this->sanitizeString($this->email_content);
	}	
}

?>