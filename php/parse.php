<?php

/**
 * Takes a filename as a GET parameter and sends the filename to the parser, then displays the output
 *
 * @version 	1.0
 * @author 		Keegan Berry
 * @since		03/03/12
 */

	require_once('classes/EmailParser.php');
	require_once('classes/Email.php');

	// at least check to make sure the file is specified

	if (!isset($_GET['file']) || $_GET['file'] == "") { 
		echo "Error, no file specified.";
		exit(); 
	}

	$filename = $_GET['file'];
	$parse = new EmailParser($filename);

?>

<!DOCTYPE html> 
<html dir="ltr" lang="en-US"> 
	<head>
		<title>Email Parser Output</title>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

		<style type="text/css">
			body {
				font-family: sans-serif;
			}
			
			dl dt {
				font-weight: bold;
			}
			
			ul {
				padding:0;
				margin-left: 25px;
			}	
			
			ul li, pre.email_text {
				list-style-type: none;
				border-left: 20px solid #ccc;
				padding-left: 15px;
			}

			.original {
				margin-top: 100px;
				background: #ccc;
				padding: 20px;
			}		
			
			h1, h2, h3 {
				text-shadow: 0 1px 1px rgba(0, 0, 0, 0.35);
			}

			h1 {
				font-size: 1.5em;
			}
			
			h2 {
				font-size: 1.4em;
			}

			h3 {
				font-size: 1em;
			}				
		</style>
	</head>
<body>

<h1>Email Parser Output</h1>

<h2>Header</h2>

<dl>

<?php

	// print out the fields and values, provided they exist
	
	if (isset($parse->email->mime))
	{
		echo "<dt>MIME Version:</dt><dd>". $parse->email->mime . "</dd>";
	}

	if (isset($parse->email->delivered_to))
	{
		echo "<dt>Delivered To:</dt><dd>". $parse->email->delivered_to . "</dd>";
	}

	if (isset($parse->email->to))
	{
		echo "<dt>To:</dt><dd>". $parse->email->to . "</dd>";
	}

	if (isset($parse->email->from))
	{
		echo "<dt>From:</dt><dd>". $parse->email->from . "</dd>";
	}

	if (isset($parse->email->cc))
	{
		echo "<dt>Cc:</dt><dd>". $parse->email->cc . "</dd>";
	}	

	if (isset($parse->email->bcc))
	{
		echo "<dt>Bcc:</dt><dd>". $parse->email->bcc . "</dd>";
	}		

	if (isset($parse->email->subject))
	{
		echo "<dt>Subject:</dt><dd>". $parse->email->subject . "</dd>";
	}

	if (isset($parse->email->date))
	{
		echo "<dt>Date:</dt><dd>". $parse->email->date . "</dd>";
	}

	if (isset($parse->email->return_path))
	{
		echo "<dt>Return Path:</dt><dd>". $parse->email->return_path . "</dd>";
	}

	if (isset($parse->email->message_id))
	{
		echo "<dt>Message ID:</dt><dd>". $parse->email->message_id . "</dd>";
	}

	if (isset($parse->email->in_reply_to))
	{
		echo "<dt>In Reply To ID:</dt><dd>". $parse->email->in_reply_to . "</dd>";
	}

	if (isset($parse->email->archived_at))
	{
		echo "<dt>archived At:</dt><dd>". $parse->email->archived_at . "</dd>";
	}

	if (isset($parse->email->auto_submitted))
	{
		echo "<dt>Auto Submitted:</dt><dd>". $parse->email->auto_submitted . "</dd>";
	}
?>

</dl>

<h2>Body</h2>

<?php 

	// print out each body text

	if (count($parse->email->getBodies()) > 0)
	{
		echo "<ul>";

		foreach($parse->email->getBodies() as $body) {
			echo "<li>";
				if (isset($body["type"]))
				{
					echo "<h3>Type " . $body["type"] . "</h3>";
				}
				echo "<pre>";
				echo $body["content"];
				echo "</pre>";
			echo "</li>";
		}

		echo "</ul>";
	}

	// if no body texts are stored in the Email, output the whole email
	else {
		echo "<p>No body text found.</p>";
	}

?>

<div class="original">
<h2>Original File</h2>
<pre>

<?php

	// show the original email
	echo $parse->getEmailAsText();

?>

</pre>
</div>

</body>
</html>